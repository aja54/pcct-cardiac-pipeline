# Photon-counting CT Cardiac Phenotyping Pipeline

Code and data accompaniment for our works focused on applying an advanced photon-counting CT imaging pipeline to the study of apolipoprotein E mouse models. Our first [paper](https://doi.org/10.1371/journal.pone.0291733) focused on the impact of diet. Our second paper is currently under review and focuses on the impact of exercise.

Copyright (C) 2022-2025, [Quantitative Imaging and Analysis Lab](https://sites.duke.edu/qial/), Duke University

## Index

1. [About](#about)
2. [Contact Information](#contact-information)
2. [Citation](#citation)
7. [Funding and Support](#funding-and-support)
9. [Licensing](#licensing)

## About

This repository contains the following information intended to supplement our PLOS ONE journal papers: ([Paper 1](https://doi.org/10.1371/journal.pone.0291733)) and (Paper 2:LINK AVAILABLE UPON PUBLICATION).
* A CSV file containing all animal IDs and biological measurements used in our statistical analyses
* The 3D imaging data used for the study. (Data are given as 3D+channel+time NIFTI files with the 4 channels representing (in this order) decomposed iodine volume, CT attenuation volume, LV segmentation (paper 1), and multichamber segmentation (paper 2). The 2 timepoints of the volumes correspond to the end diastolic and systolic phases.)
* Left ventricle segmentation Python scripts
* Multichamber cardiac segmentation scripts
* Pretrained CNN weights for both left ventricle and multichamber segmentation

## Contact Information:

This work has been published and created by our team at the Quantitative Imaging and Analysis Lab at Duke University Medical Center:

[Quantitative Imaging and Analysis Lab](https://sites.duke.edu/qial/)

If you are interested in collaborating or contributing to our research, do not hesitate to contact us.


## Citation

If you reuse any portion of our code or data in your work, please cite the following publication:

Allphin AJ, Mahzarnia A, Clark DP, Qi Y, Han ZY, et al. (2023) Advanced photon counting CT imaging pipeline for cardiac phenotyping of apolipoprotein E mouse models. PLOS ONE 18(10): e0291733. [https://doi.org/10.1371/journal.pone.0291733](https://doi.org/10.1371/journal.pone.0291733)

## Funding and Support

Funding for this work was provided by the National Institutes of Health: National Institute on Aging (RF1 AG057895, R01 AG066184, RF1AG070149, supplement RF1AG070149-01S1)


## Licensing

Copyright (C) 2022-2025, [Quantitative Imaging and Analysis Lab](https://sites.duke.edu/qial/), Duke University 

The programs and scripts contained herein are free: you can redistribute and/or modify under the terms of the Creative Commons by 4.0 license. This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. For full details of the CC by 4.0 license visit <https://creativecommons.org/licenses/by/4.0/>.
