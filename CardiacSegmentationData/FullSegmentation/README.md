# Deep Learning-based Cardiac Segmentation Code (v3)

### What's included?
The scripts included have not been refined for external use as the paths and formats are set up for my local machine. However, the basic approach and network definition should be clear enough to create your own implementation.

### How are the data formatted?
The input data for the network must be 1x128x128x128 arrays with the field of view roughly centered around the heart. 
I would suggest opening one of the example NIFTI (.nii) files to verify the coordinate system of any custom data.
The output of this network contains 9 segmentation maps corresponding to various chambers and features of the cardiac region.
