# Deep Learning-based Left Ventricle Segmentation Code (v2)

### What's included?
I have provided two basic scripts to illustrate how to (1) train and (2) implement the left ventricle segmentation.
1. "CNNtraining.py"
2. "network_implementation.py"

### How to run?
In order to run the code you will need to:
1. Set up Python environment with the needed dependencies (see "requirements.txt")
2. Verify the any data paths are correct within the desired script.

### How are the data formatted?
The input data for the network must be 1x128x128x128 arrays with the field of view roughly centered around the heart. 
I would suggest opening one of the example NIFTI (.nii) files to verify the coordinate system of any custom data.
