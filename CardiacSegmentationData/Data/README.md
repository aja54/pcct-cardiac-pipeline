# Data Format

These data files correspond to the mouse ID numbers in the spreadsheet used for statistical analysis.

Each NIFTI (.nii) file is a 128x128x128x4x2 array. The first 3 dimensions are the spatial dimensions. The 5th dimension corresponds to the diastolic and systolic phases (in that order). The 4th dimension contains 4 channels corresponding to(1) the iodine decomposition map, (2) the CT volume attenuation values, (3) the LV segmentation from our first paper and (4) the full cardiac segmentation from the second paper.
